#!/usr/bin/env bash

nohup python2 analyzer/analysis_service.py > log.txt 2> stderr.txt &
echo $! > analysis_service_pid.txt
