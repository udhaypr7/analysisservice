#!/usr/bin/env python

from package import Package
from setuptools import setup, find_packages


with open('VERSION', 'r') as f:
    version = f.read()

extras = dict()
with open('requirements.txt', 'r') as reqs:
    for line in reqs.readline():
        if 'tensorflow-gpu' in line:
            extras['tf_gpu'] = line
            break
        if 'tensorflow' in line:
            extras['tf'] = line
            break

setup(
    name='analyzer',
    version=version,
    maintainer='Omri Treidel',
    maintainer_email='omri.treidel@dragontailsystems.com',
    description='Analysis services for pizza',
    packages=find_packages(),
    package_data={'': ['VERSION']},
    include_package_data=True,
    scripts=['bin/start-service.sh',
             'run_integration_test.sh'],
    # FIXME: It's bad practice to have specific versions in here.
    # It should be the minimal versions possible.
    # FIXME: see https://packaging.python.org/discussions/install-
    # requires-vs-requirements/
    install_requires=[
        'numpy==1.14.2',
        'scipy==1.0.0',
        'scikit-image==0.13.1',
        'scikit-learn==0.19.1',
        'werkzeug==0.14.1',
        'traitlets==4.3.2',
        'matplotlib==2.2.2',
        'requests==2.18.4'
    ],
    extras_require=extras,
    cmdclass={
        "package": Package
    },
    test_suite='nose.collector',
    tests_require=['nose'],
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Programming Language :: Python :: 2.7'
        'Operating System :: POSIX :: Linux'
    ],
)
