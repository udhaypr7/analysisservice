#!/usr/bin/env bash

# start the server
python analyzer/analysis_service.py &

python analyzer/tests/integration.py

exitCode=$?

echo "Shutting down..."
curl -X POST http://0.0.0.0:5000/exit

if [ $exitCode == 0 ]
then
    echo "============================================"
    echo
    echo "Integration test PASS - Ready for deployment"
    echo
    echo "============================================"
    exit 0
else
    echo "============================================"
    echo
    echo "Integration test FAIL!"
    echo
    echo "============================================"
    exit 1
fi
