#!/usr/bin/env bash

REQS=requirements.txt
TF_VERSION=1.8.0

ldconfig -p | grep cuda > /dev/null
cuda_exit_code=$?
ldconfig -p | grep cudnn > /dev/null
cudnn_exit_code=$?


if (( $cuda_exit_code == 0 && $cudnn_exit_code == 0 ))
then
    echo "Found CUDA and CUDNN, Installng tensorflow-GPU"
    echo tensorflow-gpu==${TF_VERSION} >> ${REQS}
else
    echo "Could not find CUDA and CUDNN. Installng tensorflow"
    echo tensorflow==${TF_VERSION} >> ${REQS}
fi
