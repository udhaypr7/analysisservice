import os

import argparse
import glob
import json
import pandas as pd


parser = argparse.ArgumentParser()
parser.add_argument("input_dir", help="directory with JSON to analyze")
args = parser.parse_args()


files = glob.glob(args.input_dir + '/*.json')
rows = []
for _file in files:
    with open(_file, 'r') as f:
        _content = json.load(f)
        runtimes = _content['analysis']['runtimes']
        rows.append(runtimes.values())

data = pd.DataFrame(rows, columns=runtimes.keys())
full_report_path = os.path.join(args.input_dir, 'full_report.csv')
report_summary_path = os.path.join(args.input_dir, 'report_summary.csv')
data.to_csv(full_report_path, index=False)
data.describe().to_csv(report_summary_path, index=False)
print("Writing reports to %s, %s " % (full_report_path, report_summary_path))
