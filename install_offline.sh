#!/usr/bin/env sh

TAR_NAME=<build-tar-name>

dirname=$(basename ${TAR_NAME} .tar.gz)
tar xzf ${TAR_NAME}.tar.gz  # will create dirname
cd ${dirname}

pip install -r requirements.txt --no-index --find-links wheelhouse
pip install .
./run_integration_test.sh
