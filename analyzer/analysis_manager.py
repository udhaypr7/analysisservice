
from traitlets.config.configurable import Configurable

import analyzers


class AnalysisManager(Configurable):
    def __init__(self, analyzer_list):
        # Get dependency graph and output analyzer list

        # First, dfs to build graph
        depends = {}
        visited = set()
        Q = analyzer_list[:]
        while Q:
            u = Q.pop()
            a_class = getattr(analyzers, u)
            depends[u] = a_class.depends
            for v in depends[u]:
                if v not in visited:
                    visited.add(v)
                    Q.append(v)

        # Top sort
        in_deg = dict([(key, 0) for key in depends.keys()])
        for u, vs in depends.items():
            for v in vs:
                in_deg[v] += 1

        Q = [u for u, d in in_deg.items() if d == 0]
        res = []
        while Q:
            u = Q.pop()
            res.append(u)
            for v in depends[u]:
                in_deg[v] -= 1
                if in_deg[v] == 0:
                    Q.append(v)

        res = res[::-1]
        self.analyzer_list = res
