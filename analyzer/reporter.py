import os
import logging

import json
import requests
import time
import uuid

from matplotlib.image import imsave, imread
from tempfile import TemporaryFile
from traitlets.config.configurable import Configurable
from traitlets import Int, Unicode

from color_utils.misc import get_timestamp

import config


# TODO: Error handling
class Reporter(Configurable):
    upload_asset_url = Unicode("http://localhost:8080/cameraImage",
                               help="URL to upload assets").tag(config=True)
    upload_report_url = Unicode("http://localhost:8080/cameraReadings",
                                help="URL to upload reports").tag(config=True)
    login_url = Unicode("http://localhost:8080/login",
                        help="URL to login").tag(config=True)
    # upload_asset_url = Unicode("http://10.0.0.3:8091/cameraImage",
    # help="URL to upload assets").tag(config=True)
    # upload_report_url = Unicode("http://10.0.0.3:8091/cameraReadings",
    # help="URL to upload reports").tag(config=True)
    # login_url = Unicode("http://10.0.0.3:8091/login",
    # help="URL to login").tag(config=True)
    username = Unicode(help="username").tag(config=True)
    password = Unicode(help="password").tag(config=True)
    device_name = Unicode("device0", help="device_name").tag(config=True)
    reconnect_interval = Int(10, help="interval for reconnection attempts, "
                                      "in seconds.").tag(config=True)

    def __init__(self, **kwargs):
        super(Reporter, self).__init__(**kwargs)

        # Login - This happens only in reporter thread, so we can block
        self.logged_in = False
        self.login()

    def login(self):
        data = json.dumps({'username': self.username,
                           'password': self.password})

        try:
            resp = requests.post(url=self.login_url, data=data)
        except requests.exceptions.RequestException as e:
            logging.error("HTTP request error in login: " + str(e))
            return False
        print resp.content
        try:
            resp_json = json.loads(resp.content)
            status = resp_json['status']
            value = resp_json['value']
        except Exception as e:
            logging.error("Error parsing json(%s): %s"
                          % (resp.content, str(e)))
            return False

        if status != 'ok':
            logging.error("Failed login: " + status)
            return False

        self.token = value
        self.logged_in = True
        return True

    def process(self, reportables):
        if not self.logged_in:
            if not self.login():
                return False
        report_simple = {}
        for key, value in reportables.items():
            if not value.is_file():
                report_simple[key] = value.get()

        report = {'source': self.device_name,
                  'value': report_simple}
        report_json = json.dumps(report)

        while True:
            try:
                resp = requests.post(url=self.upload_report_url,
                                     data=report_json,
                                     headers={'token': self.token})
            except requests.exceptions.RequestException as e:
                logging.error("HTTP request error in reporting: " + str(e))
                time.sleep()
                continue
            break

        unique_id = resp.content[1: -1]  # remove quotation marks

        # upload assets
        for key, value in reportables.items():
            if value.is_file():
                if not self.upload_asset(key, value, unique_id):
                    return False

        return True

    def upload_asset(self, key, reportable, unique_id):
        fname, stream = reportable.get_stream()
        asset_dict = {'uId': unique_id,
                      'imageType': key}
        resp = requests.post(url=self.upload_asset_url,
                             data=asset_dict,
                             files={'file': (fname, stream,
                                             'application/octet-stream')},
                             headers={'token': self.token})
        print self.upload_asset_url, resp.content
        return 'successfully' in resp.content


class PassiveReporter(Configurable):
    def process(self, item):
        report_simple = {}
        reportable_id = item.id
        item.reportables['timestamps'] = JSONReportable(item.timestamps.copy())
        item.reportables['runtimes'] = JSONReportable(item.runtimes.copy())
        for key, value in item.reportables.items():
            if not value.is_file():
                report_simple[key] = value.get()
            else:
                name, s = value.get_stream()
                ext = name.split(".")[-1]

                image_name = "{reportable_id}_{reportable_item}.{extension}"\
                    .format(reportable_id=reportable_id,
                            reportable_item=key,
                            extension=ext)
                image_path = os.path.join(config.images_path, image_name)
                with open(image_path, 'wb') as file_handle:
                    file_handle.write(s.read())
                report_simple[key] = 'image:{reportable_id}_{reportable_item}'\
                    .format(reportable_id=reportable_id, reportable_item=key)
                report_simple[key + '_path'] = os.path.abspath(image_path)

        json_file_path = os.path.join(config.reports_path, item.id + '.json')
        logging.info("Writing report %s" % json_file_path)
        with open(json_file_path, 'w') as file_handle:
            json.dump(report_simple, file_handle, indent=4, sort_keys=True)

        return report_simple


class AnalysisResults:
    def __init__(self):
        self.id = uuid.uuid4().hex
        now_local, now_utc = get_timestamp()
        self.timestamps = dict(created_at_local=now_local,
                               created_at_utc=now_utc)
        self.runtimes = dict()
        self.reportables = {}
        self.analysis = {}


class Reportable:
    def is_file(self):
        return False

    def get(self):
        return ""


def image_to_str(im):
    f = TemporaryFile()
    imsave(f, im, format='png')
    f.seek(0)
    return f.read()


def str_to_image(string, trunc=True):
    f = TemporaryFile()
    f.write(string)
    f.seek(0)
    im = imread(f, format='png')
    if trunc:
        im = im[..., :3]
    return im


class ImageReportable(Reportable, Configurable):
    image_format = Unicode("png", help="Image file format to report "
                                       "(png, jpg, ...)").tag(config=True)

    def __init__(self, im, **kwargs):
        super(ImageReportable, self).__init__(**kwargs)
        self.im = im.copy()

    def __getstate__(self):
        return image_to_str(self.im)

    def __setstate__(self, data):
        self.im = str_to_image(data)

    def is_file(self):
        return True

    def get_stream(self):
        f = TemporaryFile()
        imsave(f, self.im, format=self.image_format)
        f.seek(0)
        # 'application/octet-stream'
        # data = base64.b64encode(f.read())
        # f = TemporaryFile()
        # f.write(data)
        # f.seek(0)

        return 'file.%s' % self.image_format, f


class TextReportable(Reportable):
    def __init__(self, txt):
        self.txt = txt

    def get(self):
        return self.txt


class JSONReportable(Reportable):
    def __init__(self, obj):
        # self.txt = json.dumps(obj)
        self.obj = obj

    def get(self):
        # return self.txt
        return self.obj
