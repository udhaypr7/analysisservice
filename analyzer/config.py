import os


def make_path(path):
    if not os.path.isdir(path):
        os.makedirs(path)


HOME = os.getenv('HOME')
images_path = os.path.join(HOME, 'analyzer_artifacts', 'images')
reports_path = os.path.join(HOME, 'analyzer_artifacts', 'reports')

make_path(images_path)
make_path(reports_path)
models_path = os.path.join(os.path.dirname(__file__), 'analyzers/models')

decider_model_path = os.path.join(models_path, 'decider.h5')
segmenter_model_path = os.path.join(models_path, 'segmenter.h5')

pizza_grade_params_general_path = os.path.join(
    models_path, 'pizza_grader_params_general.json')

pizza_grade_params_crust_path = os.path.join(
    models_path, 'pizza_grader_params_crust.json')

pizza_grade_params_spread_path = os.path.join(
    models_path, 'pizza_grader_params_spread.json')

pizza_grade_params_cheese_path = os.path.join(
    models_path, 'pizza_grader_params_cheese.json')

pizza_grade_params_remake_path = os.path.join(
    models_path, 'pizza_grader_params_remake.json')

dough_params_path = os.path.join(models_path, 'doughtype.json')
