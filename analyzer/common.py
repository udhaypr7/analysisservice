import numpy as np
from skimage.morphology import binary_erosion, square
from scipy.ndimage.morphology import distance_transform_cdt


def crop_image(I, mask, thickness):
    # Smooth edges of mask
    mask = binary_erosion(mask, square(thickness))
    d = distance_transform_cdt(~mask).astype(np.float32)
    alpha = 1 - (np.minimum(d, thickness * 2) / (thickness * 2))
    im = I.copy()[..., :3]
    im = np.concatenate([im, (alpha * 255).astype(np.uint8)[..., np.newaxis]],
                        -1)
    return im
