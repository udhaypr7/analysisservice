import numpy as np

from analyzer.analyzers import pizzagrader_utils


def test_mask_boundary():
    #   | 0 1 2 3 4 5 6 7        0 1 2 3 4 5 6 7
    # -------------------       -----------------
    # 0 | 1 0 1 0 0 0 0 0        1 0 1 0 0 0 0 0
    # 1 | 0 0 1 1 0 0 0 0        0 0 1 1 0 0 0 0
    # 2 | 1 1 1 1 1 0 0 0        1 1 0 0 1 0 0 0
    # 3 | 1 1 1 1 1 1 0 0        0 0 0 0 0 1 0 0
    # 4 | 1 1 1 1 1 1 1 0 -->    1 0 0 0 0 0 1 0
    # 5 | 0 1 1 1 1 1 1 0        0 1 0 0 0 0 1 0
    # 6 | 0 1 1 1 1 1 1 0        0 1 0 0 0 0 1 0
    # 7 | 0 1 1 1 1 1 0 0        0 1 0 0 0 1 0 0
    # 8 | 0 1 1 1 1 0 0 0        0 1 0 0 1 0 0 0
    # 9 | 0 1 1 1 0 0 0 0        0 1 0 1 0 0 0 0

    mask_blobs = np.zeros((10, 8), np.int)
    mask_blobs[0, [0, 2]] = 1
    mask_blobs[1, 2:4] = 1
    mask_blobs[2, 0:5] = 1
    mask_blobs[3, 0:6] = 1
    mask_blobs[4, 0:7] = 1
    mask_blobs[5, 1:7] = 1
    mask_blobs[6, 1:7] = 1
    mask_blobs[7, 1:6] = 1
    mask_blobs[8, 1:5] = 1
    mask_blobs[9, 1:4] = 1
    mask_contour = \
        pizzagrader_utils.mask_boundary(mask_blobs).astype(np.int)

    mask_expected = np.zeros((10, 8), np.int)
    mask_expected[0, [0, 2]] = 1
    mask_expected[1, [2, 3]] = 1
    mask_expected[2, [0, 1, 4]] = 1
    mask_expected[3, 5] = 1
    mask_expected[4, [0, 6]] = 1
    mask_expected[5, [1, 6]] = 1
    mask_expected[6, [1, 6]] = 1
    mask_expected[7, [1, 5]] = 1
    mask_expected[8, [1, 4]] = 1
    mask_expected[9, [1, 3]] = 1
    assert mask_expected.tolist() == mask_contour.tolist()


def test_divide_mask():
    num_sectors = 8
    mask = np.zeros((5, 5), bool)
    sectors = pizzagrader_utils.divide_mask(mask, num_sectors)
    expected_sectors = np.array([[1, 2, 2, 3, 3],
                                 [1, 1, 2, 3, 4],
                                 [8, 8, 4, 4, 4],
                                 [8, 7, 6, 5, 5],
                                 [7, 7, 6, 6, 5]])
    assert (sectors == expected_sectors).all()

    offset = np.pi/16
    sectors = pizzagrader_utils.divide_mask(mask, num_sectors, offset=offset)
    expected_sectors = np.array([[2, 2, 3, 3, 4],
                                 [1, 2, 3, 4, 4],
                                 [1, 1, 4, 5, 5],
                                 [8, 8, 7, 6, 5],
                                 [8, 7, 7, 6, 6]])
    assert (sectors == expected_sectors).all()

    center = (1, 1)
    sectors = pizzagrader_utils.divide_mask(mask, num_sectors, center=center)
    expected_sectors = np.array([[1, 2, 3, 4, 4],
                                 [8, 4, 4, 4, 4],
                                 [7, 6, 5, 5, 5],
                                 [7, 6, 6, 5, 5],
                                 [7, 6, 6, 6, 5]])
    assert (sectors == expected_sectors).all()
