import numpy as np

from analyzer.analyzers import pizzagrader


def test_get_remake_reasons():
    pizza_grader = pizzagrader.PizzaGrader()
    pizza_grader.params_remake['min_acceptable_score'] = 7
    pizza_grader.params_remake['threshold_missing_cheese'] = 4
    pizza_grader.params_remake['threshold_missing_topping'] = 5
    pizza_grader.params_general['num_sectors'] = 8

    no_remake = pizza_grader.get_remake_reasons(2, 5, 2,
                                                np.ones((2, 8), np.uint8),
                                                dict())
    assert list() == no_remake

    low_score = pizza_grader.get_remake_reasons(1, 2, 3,
                                                np.ones((2, 8), np.uint8),
                                                dict())
    assert ['Low Score'] == low_score

    score_per_sector_cheese = np.zeros((2, 8), bool)
    score_per_sector_cheese[0, 0:3] = True
    res_missing_cheese = \
        pizza_grader.get_remake_reasons(2, 5, 2, score_per_sector_cheese,
                                        dict())
    assert ['Missing Cheese'] == res_missing_cheese

    sector_topping_score = dict()
    sector_topping_score['Beef'] = np.zeros((2, 8), bool)
    sector_topping_score['Beef'][0, 0:4] = True
    res_missing_topping = \
        pizza_grader.get_remake_reasons(2, 5, 2,
                                        np.ones((2, 8), np.uint8),
                                        sector_topping_score)
    assert ['Missing Beef'] == res_missing_topping

    res_all_options = \
        pizza_grader.get_remake_reasons(1, 2, 3,
                                        score_per_sector_cheese,
                                        sector_topping_score)
    expected_all_options = ['Low Score', 'Missing Cheese', 'Missing Beef']
    assert expected_all_options == res_all_options

    sector_topping_score = dict()
    sector_topping_score['Beef'] = np.ones((2, 8), np.uint8)
    sector_topping_score['Mushroom'] = np.zeros((2, 8), np.uint8)
    res_missing_first_remake_topping = \
        pizza_grader.get_remake_reasons(2, 5, 2,
                                        np.ones((2, 8), np.uint8),
                                        sector_topping_score)
    assert ['Missing Mushroom'] == res_missing_first_remake_topping

    sector_topping_score = dict()
    sector_topping_score['Beef'] = np.zeros((2, 8), np.uint8)
    sector_topping_score['Mushroom'] = np.zeros((2, 8), np.uint8)
    res_missing_first_topping = \
        pizza_grader.get_remake_reasons(2, 5, 2,
                                        np.ones((2, 8), np.uint8),
                                        sector_topping_score)
    # Mushroom is the first entry in the dict
    assert ['Missing Mushroom'] == res_missing_first_topping
