from unittest import TestCase

from analyzer import get_premade_pizzas
from color_utils import get_toppings_rgb


class TestImport(TestCase):

    def test_premade_pizzas(self):
        """Testing the structure of the premade pizzas YML and the exact
        number of fields """
        recipes = get_premade_pizzas()
        expected_keys = set(['toppings', 'sauce', 'name'])
        self.assertEquals(len(recipes), 37)
        self.assertEquals(set(recipes[0].keys()), expected_keys)

    def test_get_toppings_rgb(self):
        """Testing the structure of the RGB values YML and the exact number
        of fields """
        rgb_values = get_toppings_rgb()
        expected_keys_lvl_0 = set(['other', 'toppings'])
        self.assertEquals(len(rgb_values['other']), 7)
        self.assertEquals(len(rgb_values['toppings']), 24)
        self.assertEquals(set(rgb_values.keys()), expected_keys_lvl_0)
        expected_keys_lvl_1 = set(['rgb', 'name'])
        self.assertEquals(set(rgb_values['other'][0].keys()),
                          expected_keys_lvl_1)
