import numpy as np

from analyzer import config
from analyzer import utils
from analyzer.analyzers import doughtype


def test_get_mask_cardboard():
    # testing:
    # - find relevant values in Lab space
    # - connect 4 corners
    image = np.zeros((4, 4, 3), np.uint8)
    image[0, 0, 0] = 183
    image[0, 3, 0] = 184
    image[3, 0, 0] = 185
    image[3, 3, 0] = 186

    image[0, 0, 1] = 147
    image[0, 3, 1] = 148
    image[3, 0, 1] = 149
    image[3, 3, 1] = 150

    image[0, 0, 2] = 109
    image[0, 3, 2] = 110
    image[3, 0, 2] = 111
    image[3, 3, 2] = 112

    labels = np.ones((4, 4), np.int)
    labels[0, 0] = 0
    labels[0, 3] = 0
    labels[3, 0] = 0
    labels[3, 3] = 0

    dough_params = utils.load_json(config.dough_params_path)
    mask_cardboard = \
        doughtype.get_mask_cardboard(image, labels,
                                     dough_params['cardboard_color_lab_width'])

    mask_expected = np.zeros((4, 4), bool)
    mask_expected[0, 0] = True
    mask_expected[0, 3] = True
    mask_expected[3, 0] = True
    mask_expected[3, 3] = True

    assert (mask_expected == mask_cardboard).all()
