import h5py

from analyzer import config


def test_decider():
    with h5py.File(config.decider_model_path, 'r') as h5:
        keys = h5.keys()
        assert set(keys) == set(['model_weights', 'normalization_params',
                                 'metadata'])


def test_segmenter():
    with h5py.File(config.segmenter_model_path, 'r') as h5:
        keys = h5.keys()
        assert set(keys) == set(['model_weights', 'normalization_params',
                                 'metadata'])
