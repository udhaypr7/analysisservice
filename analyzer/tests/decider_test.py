import numpy as np

from skimage.morphology import binary_dilation, square

from analyzer import utils
from analyzer.analyzers.decider import RECIPES, TOPPINGS
from analyzer.analyzers.decider import add_missing_toppings
from analyzer.analyzers.decider import create_toppings_report
from analyzer.analyzers.decider import prepare_toppings_report
from analyzer.analyzers.decider import _raw_scores
from color_utils.pizza_colors import PizzaClasses


PIZZA_CLASSES = PizzaClasses.default().filter('name', 'Unknown')
NUM_TOPPINGS = PIZZA_CLASSES.num_toppings


def test_toppings():
    assert NUM_TOPPINGS == 24


def test_addable():
    # 1 addable which is missing
    toppings_names = ["Beef", "Pepperoni", "Ham", "Bacon", "Italian Sausage"]
    toppings = TOPPINGS.select('name', toppings_names)\
        .filter('name', 'Pepperoni')
    probabilities = np.ones(PIZZA_CLASSES.num_classes)
    probabilities[TOPPINGS.labels] = 0.3
    probabilities[toppings.labels] = 0.7

    report_toppings = create_toppings_report(toppings.labels, probabilities)
    items_names = set(map(lambda x: x['item'], report_toppings))
    assert set(toppings.names) == items_names

    extended_labels, recipe_key = add_missing_toppings(toppings.labels)
    assert RECIPES[recipe_key].name == "BBQ Meatlovers"
    expected_labels = set(TOPPINGS.select('name', toppings_names).labels)
    assert extended_labels == expected_labels
    final_names = TOPPINGS.select('label', extended_labels).names
    assert set(final_names) == set(toppings_names)

    # lets try again with `prepare_toppings_report`
    report_toppings, recipe_key = prepare_toppings_report(probabilities)
    final_names = set(map(lambda x: x['item'], report_toppings))
    expected_names = [name for name in toppings_names]
    expected_names.remove('Pepperoni')
    expected_names.append('Pepperoni ?')
    assert final_names == set(expected_names)

    # 2 addable, both are missing
    addable_names = ["Pepperoni", "Ham"]
    addable = TOPPINGS.select('name', addable_names)
    toppings = TOPPINGS.select('name', toppings_names)\
        .filter('name', addable_names)
    probabilities = np.ones(PIZZA_CLASSES.num_classes)
    probabilities[TOPPINGS.labels] = 0.3
    probabilities[toppings.labels] = 0.7

    report_toppings = create_toppings_report(toppings.labels, probabilities)
    items_names = set(map(lambda x: x['item'], report_toppings))
    assert set(items_names) == set(toppings.names)
    extended_labels, _ = add_missing_toppings(toppings.labels, addable)
    assert extended_labels == set(toppings.labels) | set(addable.labels)

    # 2 addable, only 1 is missing
    toppings = TOPPINGS.select('name', toppings_names)\
        .filter('name', 'Pepperoni')
    probabilities = np.ones(PIZZA_CLASSES.num_classes)
    probabilities[TOPPINGS.labels] = 0.3
    probabilities[toppings.labels] = 0.7

    report_toppings = create_toppings_report(toppings.labels, probabilities)
    items_names = set(map(lambda x: x['item'], report_toppings))
    assert items_names == set(toppings.names)

    extended_labels, _ = add_missing_toppings(toppings.labels, addable)
    assert extended_labels == set(toppings.labels) | set(addable.labels)

    # simple cheese
    toppings_names = []
    toppings = TOPPINGS.select('name', toppings_names)
    probabilities = np.ones(PIZZA_CLASSES.num_classes)
    probabilities[TOPPINGS.labels] = 0.3
    probabilities[toppings.labels] = 0.7

    report_toppings = create_toppings_report(toppings.labels, probabilities)
    items_names = set(map(lambda x: x['item'], report_toppings))
    assert items_names == set(toppings.names)

    extended_labels, recipe_key = add_missing_toppings(toppings.labels)
    assert recipe_key is None
    assert extended_labels == set()


def test_no_recipe():
    # 1 addable which is missing
    toppings_names = ["Beef", "Baby Spinach", "Feta", "Prawns"]
    toppings = TOPPINGS.select('name', toppings_names)
    probabilities = np.ones(PIZZA_CLASSES.num_classes)
    probabilities[TOPPINGS.labels] = 0.3
    probabilities[toppings.labels] = 0.7
    _, recipe_key = add_missing_toppings(toppings.labels)
    assert recipe_key is None


def get_labels():
    params = dict()
    params['width'] = 40
    params['height'] = 40
    labels = np.zeros((params['height'], params['width']), int)

    cheese_label = PIZZA_CLASSES.select('name', 'Regular Cheese').labels[0]
    params['cheese_width'] = 20
    params['cheese_height'] = 20
    labels[10:30, 10:30] = cheese_label

    mask = labels == cheese_label
    params['crust_thickness'] = 2
    square_size = params['crust_thickness'] + 1
    crust_sel = binary_dilation(mask, square(square_size)) & ~mask
    labels[crust_sel] = PIZZA_CLASSES.select('name', 'Crust').labels[0]

    params['onion_width'] = 5
    params['onion_height'] = 5
    onion_x = slice(15, 15 + params['onion_width'])
    onion_y = slice(15, 15 + params['onion_height'])
    labels[onion_x, onion_y] = PIZZA_CLASSES.select('name', 'Onion').labels[0]

    params['beef_width'] = 4
    params['beef_height'] = 4
    beef_x = slice(21, 21 + params['beef_width'])
    beef_y = slice(21, 21 + params['beef_height'])
    labels[beef_x, beef_y] = PIZZA_CLASSES.select('name', 'Beef').labels[0]
    return labels, params


def test_raw_scores():
    topping_names = ['Onion', 'Beef', 'Pepperoni']
    labels, params = get_labels()

    inds = PIZZA_CLASSES.select('name', topping_names).labels
    probabilities = np.zeros(PIZZA_CLASSES.num_classes)
    probabilities[inds] = np.array([0.9, 0.86, 0.32])
    areas = np.zeros(PIZZA_CLASSES.num_classes)
    pizza_area = utils.pizza_area(labels)

    expected_onion_area = params['onion_height'] * params['onion_width']
    expected_beef_area = params['beef_height'] * params['beef_width']
    areas[inds] = np.array([expected_onion_area, expected_beef_area, 0.0])
    areas /= pizza_area

    raw_scores = _raw_scores(probabilities, labels)
    nonzero_scores = [item for item in raw_scores if float(item['score']) > 0]
    assert set(map(lambda x: x['item'], nonzero_scores)) == set(topping_names)

    onion = filter(lambda x: x['item'] == 'Onion', nonzero_scores)[0]
    beef = filter(lambda x: x['item'] == 'Beef', nonzero_scores)[0]
    pepperoni = filter(lambda x: x['item'] == 'Pepperoni', nonzero_scores)[0]

    assert abs(onion['area'] - expected_onion_area / pizza_area) < 1e-6
    assert abs(beef['area'] - expected_beef_area / pizza_area) < 1e-6
    assert pepperoni['area'] == 0.0
