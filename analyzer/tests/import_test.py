from unittest import TestCase


class TestImport(TestCase):

    def test_import_utils(self):
        from analyzer import utils
        utils.scale
        self.assertTrue(True)

    def test_import_analyzers(self):
        from analyzer import analyzers
        analyzers.Spread
        self.assertTrue(True)

    def test_import_segmenter(self):
        from analyzer.analyzers import segmenter
        segmenter.Segmenter
        self.assertTrue(True)

    def test_version(self):
        from analyzer import version
        self.assertTrue(isinstance(version, str))
