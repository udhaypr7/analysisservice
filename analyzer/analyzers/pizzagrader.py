import enum
import logging

import numpy as np
import time

from skimage import measure, morphology
from traitlets.config.configurable import Configurable

from analyzer import config
from analyzer import utils
from analyzer.reporter import JSONReportable

from Recipe import ADDABLE

import pizzagrader_utils


class CrustErosionSize(enum.Enum):
    """values were divided by nominal pizza area = 224000"""
    cheesy = 0.000223214285714
    classic = 0.000120535714286
    thin = 0.000084821428571


class RemakeReasons(enum.Enum):
    low_score = 'Low Score'
    missing_cheese = 'Missing Cheese'


def expected_crust_mask(item):
    """erode the inverse of the cardboard mask for the estimation of
    the crust mask."""

    kernel_size = int(0.5 +
                      CrustErosionSize[item.analysis['doughtype']].value *
                      item.analysis['pizza_area'])

    mask_pizza = utils.keep_main_cluster(~item.analysis['mask_cardboard'])

    # blob cannot touch the image boundary for erosion to work
    mask_pizza[0, :] = False
    mask_pizza[:, 0] = False
    mask_pizza[-1, :] = False
    mask_pizza[:, -1] = False

    return mask_pizza & ~pizzagrader_utils.erosion(mask_pizza, kernel_size)


def remove_question_mark(toppings_names):
    for ind in range(len(toppings_names)):
        if ' ?' in toppings_names[ind]:
            toppings_names[ind] = toppings_names[ind][0:-2]
    return toppings_names


def calculate_area_topping(erroneously_visible, mask_sector):
    num_masks = len(erroneously_visible)
    if num_masks == 0:
        area_topping = 0
    else:
        area_toppings = np.zeros(num_masks, np.int)
        for index_mask in range(num_masks):
            area_toppings[index_mask] = \
                (erroneously_visible[index_mask] & mask_sector).sum()

        area_topping = np.max(area_toppings)

    return area_topping


class PizzaGrader(Configurable):
    depends = ["Segmenter", "Decider", "DoughType"]

    def __init__(self, **kwargs):
        super(PizzaGrader, self).__init__(**kwargs)
        self.params_general = utils.load_json(
            config.pizza_grade_params_general_path)

        self.params_crust = utils.load_json(
            config.pizza_grade_params_crust_path)

        self.params_spread = utils.load_json(
            config.pizza_grade_params_spread_path)

        self.params_cheese = utils.load_json(
            config.pizza_grade_params_cheese_path)

        self.params_remake = utils.load_json(
            config.pizza_grade_params_remake_path)

    def update_reportable(self, item, score_crust, score_spread, score_cheese,
                          remake_reasons):
        pizza_scores = dict()
        pizza_scores['finalGrade'] = float(score_crust +
                                           score_spread +
                                           score_cheese)

        grade_breakdown = dict()
        grade_breakdown['border'] = float(score_crust)
        grade_breakdown['cheese'] = float(score_cheese)
        grade_breakdown['spread'] = float(score_spread)
        pizza_scores['gradeBreakdown'] = grade_breakdown

        grade_max_values = dict()
        grade_max_values['border'] = float(self.params_crust['perfect_score'])
        grade_max_values['cheese'] = float(self.params_cheese['perfect_score'])
        grade_max_values['spread'] = float(self.params_spread['perfect_score'])
        pizza_scores['gradeMaxValues'] = grade_max_values

        pizza_scores['remake'] = len(remake_reasons) > 0
        # FIXME - this is temporary as required in BE:
        pizza_scores['remakeThreshold'] = \
            self.params_remake['min_acceptable_score']
        pizza_scores['remakeReasons'] = remake_reasons

        item.reportables['pizzaGrade'] = JSONReportable(pizza_scores)

    def is_valid_sector(self, mask_crust, sector_labels, index_sector,
                        mask_expected_crust):
        """
        disqualify sector if crust area is too big or small

        Args:
            mask_crust (np.array): mask of crust
            sector_labels (np.array): a mask divided into sectors, with a
            running index per sector (1 <= index <= num sectors).
            index_sector (int): index of the current sector, 1 based
            mask_expected_crust (np.array): mask where crust is expected
        Returns:
            (int) 0 for disqualified sector, 1 for good
        """

        sector_mask = sector_labels == index_sector
        crust_area_sector = np.sum(sector_mask & mask_crust)

        crust_area_in_expected_mask = \
            np.sum(sector_mask & mask_crust & mask_expected_crust)

        expected_crust_area = np.sum(sector_mask & mask_expected_crust)

        min_allowed_crust_area = \
            expected_crust_area * \
            self.params_crust['percent_min_allowed_crust_area']
        max_allowed_crust_area = \
            expected_crust_area * \
            self.params_crust['percent_max_allowed_crust_area']

        if (crust_area_in_expected_mask > min_allowed_crust_area) and \
                (crust_area_sector < max_allowed_crust_area):
            return True
        else:
            return False

    def calculate_crust_score(self, item):
        num_sectors = self.params_general['num_sectors']
        offset_angle = np.pi / num_sectors

        # estimated center of the pizza is the center of the crust area:
        crust_selection = item.analysis['crust_mask']
        crust_center = utils.calculate_center_mass(crust_selection)

        mask_expected_crust = expected_crust_mask(item)

        sectors = pizzagrader_utils.divide_mask(crust_selection,
                                                num_sectors,
                                                center=crust_center,
                                                offset=0.0)

        offset_sectors = pizzagrader_utils.divide_mask(crust_selection,
                                                       num_sectors,
                                                       center=crust_center,
                                                       offset=offset_angle)

        score_per_sector = np.zeros((2, num_sectors), np.uint8)

        for index_sector in range(0, num_sectors):
            score_per_sector[0, index_sector] = \
                self.is_valid_sector(item.analysis['refined_crust_mask'],
                                     sectors, index_sector + 1,
                                     mask_expected_crust)

            score_per_sector[1, index_sector] = \
                self.is_valid_sector(item.analysis['refined_crust_mask'],
                                     offset_sectors, index_sector + 1,
                                     mask_expected_crust)

        score_crust = np.min(np.sum(score_per_sector, axis=1))
        return score_crust, sectors, offset_sectors

    def relative_area_sector(self, mask, topping_name, area_scale_factor,
                             pizza_area):
        """the relative size of the current topping in the current sector
        Args:
            mask (np.array): mask of a topping in a sector
            topping_name (str): the current topping
            area_scale_factor (float): permissive factor for relative area
            pizza_area (int): the area of the current pizza
        Returns:
            a float, 0 <= v <= 1
        """
        topping_area_sector = mask.sum()

        min_area_sector = \
            self.params_spread['target_area'][topping_name] * pizza_area / \
            self.params_general['num_sectors']
        score = topping_area_sector / (area_scale_factor * min_area_sector)

        return min(1.0, score)

    def relative_area(self, mask_topping, topping_name, area_scale_factor,
                      sectors, offset_sectors, pizza_area):

        num_sectors = self.params_general['num_sectors']
        sector_score = np.zeros((2, num_sectors), np.float)

        for ind in range(num_sectors):
            mask = mask_topping & (sectors == (ind + 1))
            sector_score[0, ind] = \
                self.relative_area_sector(mask, topping_name,
                                          area_scale_factor, pizza_area)

            rotated_mask = mask_topping & (offset_sectors == (ind + 1))
            sector_score[1, ind] = \
                self.relative_area_sector(rotated_mask, topping_name,
                                          area_scale_factor, pizza_area)

        score = np.min(np.mean(sector_score, axis=1))
        return score, sector_score

    def process_countable_topping(self, topping_mask, topping_name,
                                  area_scale_factor, sectors, offset_sectors,
                                  pizza_area):
        min_size = int(0.5 + self.params_spread['countable_minimal_size'] *
                       pizza_area)
        topping_mask = \
            morphology.remove_small_objects(topping_mask,
                                            min_size=min_size)
        relative_topping_area, sector_score = \
            self.relative_area(topping_mask,
                               topping_name,
                               area_scale_factor,
                               sectors,
                               offset_sectors,
                               pizza_area)
        _, num_blobs = measure.label(topping_mask, return_num=True)
        enough_occurrences = \
            num_blobs >= self.params_spread['countable'][topping_name]

        return relative_topping_area, enough_occurrences, sector_score

    def is_topping_above_cheese(self, topping_name, topping_mask, pizza_area):
        # a topping can be both above and below the cheese.
        # Now this is only Pepperoni but who knows:
        if topping_name in ADDABLE.names:
            size_threshold = self.params_spread['above_cheese_threshold'][
                topping_name] * pizza_area
            above_cheese = topping_mask.sum() >= size_threshold
        else:
            above_cheese = False

        return above_cheese

    def get_topping_weight(self, topping_name, above_cheese):
        if (topping_name in self.params_spread['topping_below']) and \
                (~above_cheese):
            topping_weight = 1 - self.params_spread['above_cheese_weight']
        else:
            topping_weight = self.params_spread['above_cheese_weight']

        return topping_weight

    def get_topping_relative_area(self, topping_mask, topping_name,
                                  area_scale_factor, sectors, offset_sectors,
                                  pizza_area):
        if topping_name in self.params_spread['countable']:
            relative_topping_area, enough_occurrences, sector_score = \
                self.process_countable_topping(topping_mask,
                                               topping_name,
                                               area_scale_factor,
                                               sectors,
                                               offset_sectors,
                                               pizza_area)
            if ~enough_occurrences:
                relative_topping_area = 0
        else:
            relative_topping_area, sector_score = \
                self.relative_area(topping_mask,
                                   topping_name,
                                   area_scale_factor,
                                   sectors,
                                   offset_sectors,
                                   pizza_area)

        return relative_topping_area, sector_score

    def calculate_spread_score(self, item, sectors, offset_sectors):
        toppings_names = \
            map(lambda x: x['item'], item.reportables['toppings'].get())

        num_labels = len(toppings_names)
        if (num_labels == 0) | \
                all('?' in topping_name for topping_name in toppings_names):
            return self.params_spread['perfect_score'], toppings_names, dict()

        labels = item.analysis['labels']

        weighted_relative_area = 0.0
        total_weight = 0.0

        area_scale_factor = \
            pizzagrader_utils.scaling_factor(num_labels, min_val=0.1,
                                             slope=0.2)

        pizza_area = item.analysis['pizza_area']

        sector_topping_score = dict()

        for topping_name in toppings_names:
            if '?' in topping_name:
                continue

            topping_mask = utils.topping_mask(labels, topping_name)

            above_cheese = self.is_topping_above_cheese(topping_name,
                                                        topping_mask,
                                                        pizza_area)

            topping_weight = self.get_topping_weight(topping_name,
                                                     above_cheese)
            total_weight += topping_weight

            relative_topping_area, sector_score = \
                self.get_topping_relative_area(topping_mask, topping_name,
                                               area_scale_factor, sectors,
                                               offset_sectors, pizza_area)
            weighted_relative_area += topping_weight * relative_topping_area

            if topping_name in self.params_spread['topping_above']:
                sector_topping_score[topping_name] = sector_score

        perfect_score = self.params_spread['perfect_score']
        score = \
            int(round(weighted_relative_area / total_weight * perfect_score))
        return score, toppings_names, sector_topping_score

    def is_cheese_spread_ok(self, mask_sauce, mask_sector, area_scale_factor,
                            erroneously_visible, pizza_area):
        """check the cheese spread in the given sector
        ALGORITHM
            sector is disqualified if:
            - sauce area in the sector is bigger than threshold
            - erroneously visible toppings' area is bigger than threshold
        Args:
            mask_sauce (np.array): mask of the sauce
            mask_sector (np.array): the sectors or rotated sectors
            area_scale_factor (float): factor for allowed sauce area per sector
            erroneously_visible (list): masks of the visible toppings that
            should not be visible, and can
            pizza_area (int): the area of the pizza
        Returns:
            (bool) False if sector was disqualified, True if ok
        """
        area_sauce = (mask_sauce & mask_sector).sum()
        scaled_max_area_sauce = \
            area_scale_factor * self.params_cheese['sauce_max_area_sector'] * \
            pizza_area

        area_topping = calculate_area_topping(erroneously_visible, mask_sector)
        max_area_topping = \
            pizza_area * self.params_cheese['topping_max_area_sector']

        return (area_sauce < scaled_max_area_sauce) and \
               (area_topping < max_area_topping)

    def calculate_cheese_score(self, item, sectors, offset_sectors,
                               toppings_names):
        # TODO - address Pizzas that have no cheese

        labels = item.analysis['labels']
        mask_sauce = (utils.topping_mask(labels, 'Tomato Sauce') |
                      utils.topping_mask(labels, 'BBQ Sauce'))

        if len(toppings_names) == 0:
            area_scale_factor = 1
        else:
            area_scale_factor = \
                pizzagrader_utils.scaling_factor(len(toppings_names),
                                                 min_val=0.1,
                                                 slope=0.05)

        pizza_area = item.analysis['pizza_area']

        toppings_names = remove_question_mark(toppings_names)
        # these toppings shouldn't be visible with a good cheese spread
        erroneously_visible = list()
        for topping_found in toppings_names:
            if topping_found in self.params_cheese['erroneously_visible']:
                erroneously_visible.append(
                    utils.topping_mask(labels, topping_found))

        num_sectors = self.params_general['num_sectors']
        sector_score = np.zeros((2, num_sectors), np.uint8)
        for index_sector in range(num_sectors):
            mask = sectors == (index_sector + 1)
            sector_score[0, index_sector] = \
                self.is_cheese_spread_ok(mask_sauce, mask, area_scale_factor,
                                         erroneously_visible, pizza_area)

            rotated_mask = offset_sectors == (index_sector + 1)
            sector_score[1, index_sector] = \
                self.is_cheese_spread_ok(mask_sauce, rotated_mask,
                                         area_scale_factor,
                                         erroneously_visible, pizza_area)

        score = np.min(np.sum(sector_score, axis=1))
        return score, sector_score

    def calculate_pizza_grade(self, item):

        self.pizza_contour = \
            pizzagrader_utils.mask_boundary(item.analysis['mask_cardboard'])

        score_crust, sectors, offset_sectors = self.calculate_crust_score(item)
        score_spread, toppings_names, sector_topping_score = \
            self.calculate_spread_score(item, sectors, offset_sectors)
        score_cheese, score_per_sector_cheese = \
            self.calculate_cheese_score(item, sectors, offset_sectors,
                                        toppings_names)
        return \
            score_crust, score_spread, score_cheese, score_per_sector_cheese, \
            sector_topping_score

    def get_remake_reasons(self, score_crust, score_spread, score_cheese,
                           score_per_sector_cheese, sector_topping_score):
        """the reasons for pizza remake

        ALGORITHM
            1. total_score < threshold
            2. cheese is missing from 2 (non adjacent) slices
            3. any topping above cheese is missing from at least 2 slices

        Args:
            score_crust (int) - crust score
            score_spread (int) - spread score
            score_cheese (int) - cheese score
            score_per_sector_cheese (np.array) - the cheese score per sector
            for the two division options.
            sector_topping_score (dict) - the topping score per sector
            for each topping above the cheese, for the two division
            options we use

        Returns:
            list: the remake reasons. empty if not to remake
        """

        remake_reasons = list()
        if (score_crust + score_spread + score_cheese) < \
                self.params_remake['min_acceptable_score']:
            remake_reasons.append(RemakeReasons.low_score.value)

        num_sectors = self.params_general['num_sectors']
        if np.min(np.sum(score_per_sector_cheese, axis=1)) < \
                (num_sectors - self.params_remake['threshold_missing_cheese']):
            remake_reasons.append(RemakeReasons.missing_cheese.value)

        threshold = self.params_remake['threshold_missing_topping']
        for topping in sector_topping_score:
            sector_score = sector_topping_score[topping]
            if (np.sum(sector_score[0, :] == 0) >= threshold) or \
                    (np.sum(sector_score[1, :] == 0) >= threshold):
                remake_reasons.append('Missing ' + topping)
                break

        return remake_reasons

    def process(self, item):
        t_start = time.time()
        logging.info('PizzaGrader: processing item %s' % item.id)
        score_crust, score_spread, score_cheese, score_per_sector_cheese, \
            sector_topping_score = self.calculate_pizza_grade(item)
        remake_reasons = \
            self.get_remake_reasons(score_crust, score_spread, score_cheese,
                                    score_per_sector_cheese,
                                    sector_topping_score)
        self.update_reportable(item, score_crust, score_spread, score_cheese,
                               remake_reasons)

        runtime = utils.toc(t_start)
        logging.info('PizzaGrader: done(%.2f)' % runtime)
        return item
