import logging
import os

import time
import numpy as np
import tensorflow as tf

from traitlets.config.configurable import Configurable

from analyzer import utils
from analyzer.reporter import JSONReportable


def nearest_resize(im, shp):
    H, W = shp
    h_slice = 0.5 + np.linspace(0, im.shape[0] - 1, H)
    w_slice = 0.5 + np.linspace(0, im.shape[1] - 1, W)
    return im[h_slice.astype(np.int64)][:, w_slice.astype(np.int64)]


class Spread(Configurable):
    depends = ["Segmenter"]

    def __init__(self, **kwargs):
        super(Spread, self).__init__(**kwargs)
        models_dir = os.path.join(os.path.dirname(__file__), 'models')
        model_path = os.path.join(models_dir, 'spread.pb')
        graph_def = tf.GraphDef()
        with tf.gfile.GFile(model_path, "rb") as f:
            graph_def.ParseFromString(f.read())

        with tf.Graph().as_default() as graph:
            self.mask_ph = tf.placeholder(tf.int32, [None, 128, 128],
                                          name="new_in")
            tf.import_graph_def(
                graph_def,
                # usually, during training you use queues, but at inference
                # time use placeholders
                # this turns into "input
                input_map={"InputData/X:0": self.mask_ph},
                return_elements=None,
                # if input_map is not None, needs a name
                name="spread",
                op_dict=None,
                producer_op_list=None
            )

            self.sess = tf.Session(graph=graph)
            self.output_node = self.sess.graph.get_tensor_by_name(
                "spread/out:0")
            self.sess.run(tf.global_variables_initializer())

    def process(self, item):
        if 'label_small' not in item.analysis:
            return
        logging.info('Spread: process')
        t0 = time.time()

        # Preprocess mask
        mask = item.analysis['label_small']
        i_s, j_s = np.nonzero(mask)
        i0, i1 = np.min(i_s), np.max(i_s)+1
        j0, j1 = np.min(j_s), np.max(j_s)+1
        mask = mask[i0:i1, j0:j1].astype(np.int32)
        mask = nearest_resize(mask, (128, 128))

        # Run
        res = self.sess.run(self.output_node, feed_dict={
            self.mask_ph: mask[np.newaxis]})[0]
        res = res[0]

        # Set results
        item.analysis['spread'] = res
        item.reportables['divisionPenalty'] = JSONReportable(str(float(res)))

        runtime = utils.toc(t0)
        logging.info('Spread: done(%.2f)' % runtime)
        return item
