import logging
import os

import numpy as np
import tensorflow as tf

from scipy.misc import imresize
from traitlets.config.configurable import Configurable

from analyzer import utils
from analyzer.reporter import JSONReportable


class Burnt(Configurable):
    depends = ["Segmenter"]

    def __init__(self, **kwargs):
        super(Burnt, self).__init__(**kwargs)

        models_dir = os.path.join(os.path.dirname(__file__), 'models')
        model_path = os.path.join(models_dir, 'burnt.pb')
        graph_def = tf.GraphDef()
        with tf.gfile.GFile(model_path, "rb") as f:
            graph_def.ParseFromString(f.read())

        with tf.Graph().as_default() as graph:
            self.image_ph = tf.placeholder(tf.uint8, [None, None, None, 3],
                                           name="new_image_in")
            self.mask_ph = tf.placeholder(tf.bool, [None, None, None, 1],
                                          name="new_mask_in")
            tf.import_graph_def(
                graph_def,
                # usually, during training you use queues,
                # but at inference time use placeholders
                # this turns into "input
                input_map={"image_in:0": self.image_ph,
                           "mask_in:0": self.mask_ph},
                return_elements=None,
                # if input_map is not None, needs a name
                name="burnt",
                op_dict=None,
                producer_op_list=None
            )

            self.sess = tf.Session(graph=graph)
            self.output_node = self.sess.graph.get_tensor_by_name(
                "burnt/out:0")
            self.sess.run(tf.global_variables_initializer())

    def process(self, item):
        if 'im_small' not in item.analysis:
            return
        logging.info('Burnt: process')
        t0 = utils.tic()
        im = item.analysis['im_small']
        mask = item.analysis['label_small']

        i_s, j_s = np.nonzero(mask)
        i0, i1 = np.min(i_s), np.max(i_s)+1
        j0, j1 = np.min(j_s), np.max(j_s)+1
        mask = mask[i0:i1, j0:j1].astype(np.uint8)
        im = im[i0:i1, j0:j1]
        mask = imresize(mask, (512, 512), 'nearest')
        mask = mask == 1
        im = imresize(im, (512, 512))

        res = self.sess.run(self.output_node, feed_dict={
            self.image_ph: im[np.newaxis],
            self.mask_ph: mask[np.newaxis, :, :, np.newaxis]})[0]
        res = res[0]-res[1]
        item.analysis['burnt'] = res

        item.reportables['burnt'] = JSONReportable('true' if res > 0
                                                   else 'false')
        item.reportables['burnt_score'] = JSONReportable(float(res))

        runtime = utils.toc(t0)
        logging.info('Burnt: done(%.2f)' % runtime)
        return item
