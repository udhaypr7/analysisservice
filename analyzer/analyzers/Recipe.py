from collections import namedtuple

from analyzer import get_premade_pizzas
from color_utils.pizza_colors import PizzaClasses

TOPPINGS = PizzaClasses.default().get_toppings()
ADDABLE = TOPPINGS.select('name', 'Pepperoni')

Recipe = namedtuple("Recipe", ["toppings", "name", "sauce"])


def get_recipes():
    recipes = dict()
    for pizza in get_premade_pizzas():
        labels = tuple(TOPPINGS.select('name', pizza['toppings']).labels)
        recipes[labels] = Recipe(**pizza)
    return recipes
