import logging

import h5py
import itertools
import numpy as np
import time

from keras.models import load_model
from traitlets.config.configurable import Configurable
from tensorflow import Graph, Session

import color_utils

from analyzer import config
from analyzer import utils
from analyzer.reporter import JSONReportable
from analyzer.utils import KerasModel
from color_utils.decider_features import compute_features, non_topping_labels
from color_utils.pizza_colors import PizzaClasses

from Recipe import get_recipes, TOPPINGS, ADDABLE


PIZZA_CLASSES = PizzaClasses.default().filter('name', 'Unknown')
NUM_CLASSES = PIZZA_CLASSES.num_classes
RECIPES = get_recipes()


class Decider(Configurable):
    depends = ["Segmenter"]

    def __init__(self, **kwargs):
        super(Decider, self).__init__(**kwargs)

        with h5py.File(config.decider_model_path, 'r') as decider_h5:
            self.train_mean = decider_h5['/normalization_params/mean'][()]
            self.train_std = decider_h5['/normalization_params/std'][()]
            expected_version = decider_h5['metadata/color_utils_version'][()]
            utils.validate_color_utils_version(color_utils.version,
                                               expected_version)
            # validate segmenter that segmenter versions are compatible
            with h5py.File(config.segmenter_model_path, 'r') as segmenter_h5:
                msg = 'Segmenter UUID is not compatible between Decider and ' \
                      'Segmenter'
                utils.compare_ids(decider_h5['metadata/segmenter_uuid'][()],
                                  segmenter_h5['metadata/uuid'][()], msg)

        graph = Graph()
        with graph.as_default():
            session = Session()
            with session.as_default():
                _model = load_model(config.decider_model_path)
                self.keras_model = KerasModel(graph, session, _model)

    def _predict(self, features):
        """ A wrapper around `model.predict`. It's necessary because the model
        is loaded on one thread and used for prediction on another.

        see https://github.com/keras-team/keras/issues/2397 for details

        Args:
            features (np.array): hand crafted features for classification

        Returns:
            np.array: class probabilities

        Note:
            Adds and removes the `batch_size` dimension required by Keras
        """
        with self.keras_model.graph.as_default():
            with self.keras_model.session.as_default():
                features = features[np.newaxis, ...]
                probabilities = self.keras_model.model.predict(features)
                return probabilities.squeeze(axis=0)

    def process(self, item):
        if 'pr_small' not in item.analysis:
            return

        t0 = time.time()
        logging.info('Decider: process item %s' % item.id)

        features = compute_features(item.analysis['pr_small'],
                                    item.analysis['label_small'])
        features = np.delete(features, list(non_topping_labels), axis=0)
        features = utils.scale(features, self.train_mean, self.train_std)
        # the network wants all the features as a single vector
        features = features.reshape(features.size, 1)
        probabilities = np.zeros(PIZZA_CLASSES.num_classes)
        probabilities[TOPPINGS.labels] = self._predict(features)

        report_toppings, recipe_key = prepare_toppings_report(probabilities)
        raw_scores = _raw_scores(probabilities, item.analysis['label_small'])
        item.reportables['toppings'] = JSONReportable(report_toppings)
        item.reportables['toppings_scores'] = JSONReportable(raw_scores)

        if recipe_key is not None:
            recipe = RECIPES[recipe_key]._asdict()
            item.reportables['recipe'] = JSONReportable(recipe)

        runtime = utils.toc(t0)
        logging.info('Decider: done(%.2f)' % runtime)
        return item


def add_missing_toppings(toppings_labels, addable=ADDABLE):
    """ Add unseen toppings if they make up the current list to a known recipe

    Args:
        toppings_labels (iterable): labels of seen/found toppings
        addable (PizzaClasses): addable toppings

    Returns:
        set: union of the seen and some or all the addable toppings labels
        tuple or None: key of the recipe for RECIPES dictionary
    """
    if len(toppings_labels) < 1:
        return set(), None

    existing_labels = set(toppings_labels) - non_topping_labels
    num_addable = addable.num_toppings
    # start with the most inclusive combination
    for number in reversed(range(1, num_addable + 1)):
        for combination in itertools.combinations(addable.labels, number):
            extended_labels = existing_labels | set(list(combination))
            recipe_key = tuple(sorted(extended_labels))
            if recipe_key in RECIPES:
                return extended_labels, recipe_key
    return existing_labels, None


def create_toppings_report(toppings_labels, probabilities,
                           question_mark=False):
    """

    Args:
        toppings_labels (iterable): labels of the toppings
        probabilities (np.array): probabilities of all classes
        question_mark (bool): flag to add '?' for addable toppings

    Returns:
        list[dict]: sorted list of topping items
    """
    if len(toppings_labels) < 1:
        return []
    toppings_names = TOPPINGS.select('label', toppings_labels).names
    toppings_probs = probabilities[toppings_labels]
    report_toppings = []
    for topping_name, probability in zip(toppings_names, toppings_probs):
        topping_name = topping_name + ' ?' if question_mark else topping_name
        row = {'item': topping_name, 'fill': str(probability)}
        report_toppings.append(row)
    return sorted(report_toppings, key=lambda x: x['fill'], reverse=True)


def prepare_toppings_report(probabilities):
    """

    Args:
        probabilities (np.array): probability per class (non toppings included)

    Returns:
        list[dict]: sorted list of key value pairs for each topping
        tuple(int): sorted topping labels which is a key to the recipes
    """
    # Always going to be 0.5 because that what the network is optimizing for
    threshold = 0.5
    class_labels = np.nonzero(probabilities > threshold)[0]
    toppings_labels = list(set(class_labels) - non_topping_labels)
    report_toppings = create_toppings_report(toppings_labels, probabilities)
    extended_labels, recipe_key = add_missing_toppings(toppings_labels)
    added_labels = list((extended_labels - set(toppings_labels)))
    report_toppings += create_toppings_report(added_labels, probabilities,
                                              question_mark=True)
    return report_toppings, recipe_key


def _toppings_area(labels):
    """ quick way to compute relative area for all toppings"""
    pizza_area = utils.pizza_area(labels)
    areas = np.bincount(labels.flatten(), minlength=NUM_CLASSES) / pizza_area
    areas = np.delete(areas, list(non_topping_labels))
    return areas


def _raw_scores(probabilities, labels):
    """

    Args:
        probabilities (np.array): class probabilities
        labels (np.array): segmented image (height x width)

    Returns:
        list[dict]: name, area and score for each topping
    """
    raw_scores = []
    for topping, area in zip(TOPPINGS.all, _toppings_area(labels)):
        item = dict(item=topping.name,
                    score=probabilities[topping.label],
                    area=area)
        raw_scores.append(item)
    return raw_scores
