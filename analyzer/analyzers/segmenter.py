import logging

import h5py
import numpy as np
import time

from keras.models import load_model
from matplotlib import pyplot as plt
from scipy import ndimage
from skimage.measure import label
from scipy.misc import imresize
from scipy.ndimage.morphology import binary_fill_holes
from tensorflow import Graph, Session
from traitlets.config.configurable import Configurable
from traitlets import Float

import color_utils

from analyzer import utils
from analyzer import config
from analyzer.common import crop_image
from analyzer.reporter import ImageReportable
from analyzer.utils import KerasModel
from color_utils.pizza_colors import PizzaClasses


NUM_CLASSES = PizzaClasses.default().filter('name', 'Unknown').num_classes


# TODO: preload weights
class Segmenter(Configurable):
    # TODO: clean up. topping_area_bound not used, line_thickness hard coded
    line_thickness = Float(5., "Contour line thickness").tag(config=True)
    depends = []

    def __init__(self, **kwargs):
        super(Segmenter, self).__init__(**kwargs)

        with h5py.File(config.segmenter_model_path, 'r') as h5:
            self.train_mean = np.array(h5['/normalization_params/mean'])
            self.train_std = np.array(h5['/normalization_params/std'])
            expected_version = h5['metadata/color_utils_version'][()]
            utils.validate_color_utils_version(color_utils.version,
                                               expected_version)

        graph = Graph()
        with graph.as_default():
            session = Session()
            with session.as_default():
                _model = load_model(config.segmenter_model_path)
                self.keras_model = KerasModel(graph, session, _model)

    def get_contour_img(self, image, labels, num_classes, thickness=None):
        # TODO: docstring to explain the logic
        if thickness is None:
            thickness = self.line_thickness
        cols = plt.cm.hsv(np.linspace(0, 1, num_classes + 1))[:-1, :3]
        contour_image = image.copy()
        for i in range(num_classes):
            mask = labels == i
            cntmask = ~mask
            cntmask[:-thickness, :-thickness] |= ~mask[thickness:, thickness:]
            cntmask[:-thickness, thickness:] |= ~mask[thickness:, :-thickness]
            cntmask[thickness:, :-thickness] |= ~mask[:-thickness, thickness:]
            cntmask[thickness:, thickness:] |= ~mask[:-thickness, :-thickness]
            cntmask &= mask
            # cntmask = (d < thickness) & (d>0)
            for coli in range(3):
                contour_image[..., coli][cntmask] = cols[i][coli]
        return contour_image

    def _predict(self, image):
        """ A wrapper around `model.predict`. It's necessary because the model
        is loaded on one thread and used for prediction on another.

        see https://github.com/keras-team/keras/issues/2397 for details

        Args:
            image (np.array): image to segment

        Returns:
            np.array: class probabilities

        Note:
            adds and removes the batch_size dimension required by Keras
        """
        with self.keras_model.graph.as_default():
            with self.keras_model.session.as_default():
                image = image[np.newaxis, ...]
                probabilities = self.keras_model.model.predict(image)
                return probabilities.squeeze(axis=0)

    def doAll(self, image):
        # TODO: replace with tiles, 64 will disappear then
        height, width = image.shape[:2]
        new_height = (height//64) * 64
        new_width = (width//64) * 64
        im_orig = image.copy()
        image = imresize(image, [new_height, new_width])

        image = utils.scale(image, self.train_mean, self.train_std)
        class_probability = self._predict(image)
        labels = np.argmax(class_probability, axis=-1)  # with possible holes

        # Fill holes
        filled_foreground = binary_fill_holes(labels > 0)
        non_bg_labels = np.argmax(class_probability[..., 1:], -1) + 1
        non_bg_labels[~filled_foreground] = 0

        # FIXME: is this still necessary with the new model?
        labels = utils.nearest_resize(non_bg_labels, im_orig.shape)
        class_probability = utils.nearest_resize(class_probability,
                                                 im_orig.shape)
        contours_image = self.get_contour_img(im_orig/255., labels,
                                              NUM_CLASSES, 3)
        contours_image = (contours_image * 255).astype('uint8')
        return im_orig, contours_image, labels, class_probability

    def process(self, item):
        t0 = time.time()
        logging.info('Segmenter: processing item %s' % item.id)

        im = item.analysis['im']
        original_image, contours_image, labels, probabilities = self.doAll(im)
        logging.info('Segmenter: finalize')

        item.analysis['im_sized'] = original_image.copy()
        # FIXME: look into usage. possibly remove
        item.analysis['labels'] = labels
        item.analysis['mask'] = labels != 0
        item.analysis['class_probabilities'] = probabilities

        # TODO: Empty seg - put before adding data to item.analysis
        if not np.any(labels != 0):
            logging.info('Segmenter: failed(%.2f)' % (time.time() - t0))
            del item.analysis['labels']
            del item.analysis['im_sized']
            return item

        # TODO: put into a function
        # trim the background from the reportables
        cluster_labels = label(labels != 0)
        cluster_size = np.bincount(cluster_labels.ravel())[1:]
        pizza_mask = cluster_labels == np.argmax(cluster_size) + 1
        pizza_bbox = ndimage.find_objects(pizza_mask)[0]
        # trim labels
        small_labels = labels.copy()
        small_labels[~pizza_mask] = 0
        small_labels = small_labels[pizza_bbox]

        # set all the contours in the background to 0
        small_contours = contours_image.copy()
        small_contours[~pizza_mask] = 0
        small_contours = small_contours[pizza_bbox]
        small_contours = crop_image(small_contours, small_labels != 0, 3)

        small_probabilities = probabilities[pizza_bbox]
        small_image = original_image[pizza_bbox]

        item.analysis['im_small'] = small_image
        item.analysis['label_small'] = small_labels
        item.analysis['pr_small'] = small_probabilities

        small_image_cropped = crop_image(small_image, small_labels != 0, 3)

        item.reportables['original'] = ImageReportable(small_image_cropped)
        item.reportables['labels'] = ImageReportable(item.analysis['labels'])
        item.reportables['seg'] = ImageReportable(small_contours)

        runtime = utils.toc(t0)
        logging.info('Segmenter: done(%.2f)' % runtime)
        return item
