import logging

import pickle
import uuid

from analyzer.reporter import JSONReportable

from traitlets.config.configurable import Configurable


class Debug(Configurable):
    depends = ["Segmenter", "Divider"]

    def __init__(self, **kwargs):
        super(Debug, self).__init__(**kwargs)
        logging.info('Debug: Setting up')
        logging.info('Debug: Setting up finished')

    def process(self, item):
        logging.info('Debug: process')
        item_filename = uuid.uuid1().hex
        with open('items/%s.pkl' % item_filename, 'wb') as f:
            pickle.dump(item.analysis, f)
        item.reportables['item_dbg'] = JSONReportable(item_filename)
        logging.info('Debug: Done')
        return item
