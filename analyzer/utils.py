import os

import glob
import json
import numpy as np
import time

from collections import namedtuple, defaultdict
from skimage import measure

from color_utils.pizza_colors import PizzaClasses

PI = np.pi

KerasModel = namedtuple('KerasModel', ['graph', 'session', 'model'])


def nearest_resize(tensor, new_shape):
    H, W = new_shape[0:2]
    inds_height = (0.5 + np.linspace(0, tensor.shape[0]-1, H)).astype(np.int32)
    inds_width = (0.5 + np.linspace(0, tensor.shape[1]-1, W)).astype(np.int32)
    return tensor[inds_height][:, inds_width]


def group_outputs(files_path):
    """

    Args:
        files_path (str): directory with files to group

    Returns:
        dict[list[str]]: a mapping between the uuid and files

    """
    ids = defaultdict(list)
    for file_name in glob.glob(files_path + '/*'):
        basename = os.path.basename(file_name)
        _id = basename.split('_')[0]
        ids[_id].append(file_name)
    return ids


def scale(input, mean, std, eps=1e-10):
    return (input - mean)/(std + eps)


def topping_mask(labels, topping_name):
    """Creates a mask of give topping

    Args:
        labels (np.array): image size array with class label for every pixel
        topping_name (str): name of a topping

    Returns:
        np.array: binary mask for the topping
    """
    pizza_classes = PizzaClasses.default()
    topping_label = pizza_classes.select('name', topping_name).labels[0]
    return labels == topping_label


def topping_area(labels, topping_name, relative=True):
    """

    Args:
        labels (np.array): image size array with class label for every pixel
        topping_name (str): name of a topping
        relative (bool): if True returns area relative to pizza area

    Returns:
        absolute or relative area of a topping
    """

    topping_area = float(topping_mask(labels, topping_name).sum())
    if relative:
        return topping_area/max(pizza_area(labels), 1.0)  # avoid dividing by 0
    return topping_area


def pizza_area(labels, pizza_classes=PizzaClasses.default()):
    bg_label = pizza_classes.select('name', 'Background').labels[0]
    return float((labels != bg_label).sum())


class ColorUtilsVersionError(Exception):
    pass


def validate_color_utils_version(version, expected_version):
    """Validate major version of color_utils (breaking changes)"""
    major = version.split('.')[0]
    expected_major = expected_version.split('.')[0]
    if major != expected_major:
        msg = "color_utils.version = %s not compatible with expected version" \
              " %s".format(version, expected_version)
        raise ColorUtilsVersionError(msg)


def compare_ids(id_1, id_2, msg):
    if id_1 != id_2:
        raise Exception(msg)


def load_json(path_to_json):
    with open(path_to_json, 'r') as file_handle:
        data_from_file = json.load(file_handle)

    return data_from_file


def calculate_mask_center(mask):
    return np.array(mask.shape) // 2


def calculate_center_mass(mask):
    """find the center of mass of the mask. If the mask is all 0 return
    the center of the mask

    Args:
        mask (np.array): 0 for background

    Returns:
        (np.array) of the [row, col].
    """

    if mask.any():
        row_sector, col_sector = np.where(mask > 0)
        return np.array([int(row_sector.mean()),
                         int(col_sector.mean())])
    return calculate_mask_center(mask)


def complex_grid(image, center=None):
    """ generate a complex grid around the image center

    Args:
        image (np.array): an image
        center (iterable): center of the grid. If non will be the center of
            the image

    Returns:
        np.array: complex grid
    """
    height, width = image.shape[:2]
    if center is None:
        center = (width // 2, height // 2)
    x = np.arange(width) - center[0]
    y = np.arange(height) - center[1]
    X, Y = np.meshgrid(x, y)
    return X + 1j*Y


def compute_angles(mask, center=None, offset=0.0):
    """ Angles [0, 2 PI) of the True value in a binary mask

    Args:
        mask (np.array): an mask
        center (iterable): center of the grid. If non will be the center of
            the image
        offset (float): offset angle with respect to the x axis

    """
    Z = complex_grid(mask, center=center) * np.exp(1j * offset)
    if not isinstance(mask.dtype, bool):
        mask = mask.astype(bool)
    angles = np.angle(Z) + PI
    return angles[mask]


def bin_angles(mask, num_bins, center=None):
    """

    Args:
        mask (np.array):
        num_bins (int): number of bins. Bins edges will be num_bins + 1
        center (iterable): row and col indices for the center. If non will be
            the center of the mask

    Returns:
        np.array: The values of the histogram.
        np.array: bin edges is of length num_bins + 1.
    """
    angles = compute_angles(mask, center=center)
    bins = np.linspace(0, 2 * PI, num_bins + 1)
    return np.histogram(angles, bins=bins)


def keep_main_cluster(mask):
    """ Find the largest connected component

    Args:
        mask: binary mask

    Returns:
        binary mask of the main cluster as foreground
    """
    labels = measure.label(mask)
    max_area = 0.0
    best_label = -1
    for prop in measure.regionprops(labels):
        if prop.area > max_area:
            best_label = prop.label
            max_area = prop.area
    return labels == best_label


def tic():
    return time.time()


def toc(t_start):
    return round(tic() - t_start, 3)
